/**
 * *****
 * File: helloworld.js
 * Project: curso-nodejs-mysql-luiztools
 * File Created: Tuesday, 23 February 2021 10:57:25
 * Author: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br)
 * -----
 * Last Modified: Tuesday, 23 February 2021 10:59:02
 * Modified By: Marcos Antônio Barbosa de Souza (<marcantech@uol.com.br>)
 * -----
 * Copyright (c) 2019 - 2021 All rights reserved, Marcant Tecnologia
 * -----
 * Description:
 *              Hello World do Livro em NodeJS com MySQL - Luiztools
 * *****
 */
console.log('Hello World!');